# My rofi dotfiles

[![GPLv3 license](https://img.shields.io/gitlab/license/appuchia-dotfiles/rofi?style=flat-square)](https://gitlab.com/appuchia-dotfiles/rofi/-/blob/master/LICENSE)
[![Author](https://img.shields.io/badge/Project%20by-Appu-9cf?style=flat-square)](https://gitlab.com/appuchia)

## License

This project is licensed under the [GPLv3 license](https://gitlab.com/appuchia-dotfiles/rofi/-/blob/master/LICENSE).

Coded with 🖤 by Appu
